* AARKTA -- ARM ARKTA
  Yes, it's yet another Operating System I'm making.  Currently
  targeting Raspberry Pi 2, but once I buy the actual thing, that
  might change.
* Requirements to build
  - GNU Binutils built for ~arm-none-eabi~.
  - GNU Make
  - Probably a shell that is somewhat POSIXy, but I'm not sure lmao
    (because Makefile uses ~mkdir~ and ~[ ]~ testing)
* Requirements to run
  Either
  1. A Raspberry Pi 2, put the ~kernel7.img~ on your prepared Raspbian
     SD card also deleting or renaming any other ~kernel*.img~ files
     already located there.
  2. ~qemu-system-arm~, but I believe ~qemu-system-aarch64~ should
     work as well (change that in Makefile).
* Ok sure idc, gimme command-lines I can copy&paste
  - ~make~ :: Builds the kernel, and also creates an ASM listing of
              it.
  - ~make qemu~ :: Builds the kernel (if needed) and runs it in Qemu.
  - ~make clean~ :: Deletes everything made by any other ~make~
                    invocations.
* Licensing
  Everything's licensed under GPLv3.0 or a later version as published
  by the Free Software Foundation, you know the drill.

  That being said, this software can not be considered fully libre as
  it depends on non-free firmware for Raspberry Pi (which is why you
  have to use a prepared Raspbian SD card which has the firmware).
