/* init.s -- Raw entry point to kernel. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.section .text.boot

	.globl _start
	.globl halt

/* Entry point
 * r0 = 0
 * r1 = machine id (0xC42)
 * r2 = atags start (0x100)
 *
 * Those will be preserved up to Main.
 */
_start:
	/* If machine id isn't correct, halt. */
	ldr r3, =0xC42
	cmp r1, r3
	bne Halt

	/* Shut down the extra cores */
	mrc p15, 0, r5, c0, c0, 5
	and r5, r5, #3
	cmp r5, #0
	bne Halt

	/* Clear out bss. */
	ldr r4, =__bss_start
	ldr r9, =__bss_end
	mov r5, #0
	mov r6, #0
	mov r7, #0
	mov r8, #0
	b 2$

1$:
	/* Store zeroes at r4. */
	stmia r4!, {r5-r8}

2$:
	/* If below bss_end, loop. */
	cmp r4, r9
	blo 1$

	/** Setup SCTLR. */
	/* Get current value. */
	mrc p15, 0, r3, c1, c0, 0

	/* Clear flags:
	 * TE (exceptions in ARM state)
	 * EE (exceptions little endian)
	 * Z  (branch prediction disabled)
	 * SW (SWP and SWPB instructions are disabled)
	 * M  (MMU disabled) */
	ldr r4, =(~((1<<30)|(1<<25)|(1<<11)|(1<<10)|1))
	and r3, r3, r4

	/* Set flags:
	 * I (instruction cache enabled)
	 * C (data & unified cache enabled)
	 * A (alignment check enabled) */
	ldr r4, =((1<<12)|(1<<2)|(1<<1))
	orr r3, r3, r4

	/* Set the new SCTLR value. */
	mcr p15, 0, r3, c1, c0, 0

	/* Setup the stack */
	/* TODO: seperate user-space stack */
	ldr r5, =_start
	mov sp, r5
	msr sp_usr, r5

	/* Go to user-space. */
	cps #(1<<4)

	/* Call main. */
	bl Main

	/* Halt. */
	b Halt
