/* main.s -- The "pretty" kernel entry point. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.section .text

	.globl Main

Main:
	/* Initialization */
	bl InitUART

	ldr r0, =SendChar
	bl SetWriteCharProc

	ldr r0, =RecvChar
	bl SetReadCharProc

	/* Initialization done. */
	ldr r0, =hello_world
	bl WriteStringZ

0$:	bl ReadChar
	bl WriteChar
	b 0$


	.section .rodata

	.align 4
hello_world:
	.asciz "Hello, World!\r\n"
