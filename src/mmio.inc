/* mmio.inc -- Memory Mapped I/O port base include file. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.ifndef MMIO_INC_
	.set MMIO_INC_, 1

	/* RPI 1 */
	/* .set MMIO_BASE 0x20000000 */

	/* RPI 2&3 */
	.set MMIO_BASE, 0x3F000000

	/* RPI 4 */
	/* .set MMIO_BASE 0xFE000000 */

	.endif			/* MMIO_INC_ */
