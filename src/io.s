/* io.s -- Input/Output abstractions. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.section .text

	.globl SetReadCharProc
	.globl SetWriteCharProc

	.globl ReadChar
	.globl WriteChar
	.globl WriteNewLine
	.globl WriteSInt
	.globl WriteStringZ
	.globl WriteUInt
	.globl WriteUIntBinary

SetReadCharProc:
	ldr r1, =ReadCharProc
	str r0, [r1]
	mov pc, lr


SetWriteCharProc:
	ldr r1, =WriteCharProc
	str r0, [r1]
	mov pc, lr


ReadChar:
	ldr r0, =ReadCharProc
	ldr r0, [r0]
	bx r0


WriteChar:
	ldr r1, =WriteCharProc
	ldr r1, [r1]
	bx r1


WriteNewLine:
	push {lr}
	mov r0, #'\r'
	bl WriteChar

	pop {lr}
	mov r0, #'\n'
	b WriteChar


WriteSInt:
	push {lr}

	mov r2, r0
	ldr r0, =buffer
	mov r1, #buffer_size
	bl SIntToString

	pop {lr}
	b WriteStringZ


WriteStringZ:
	push {r4, lr}
	mov r4, r0

0$:	ldrb r0, [r4], #1
	cmp r0, #0
	beq 1$

	bl WriteChar
	b 0$

1$:	pop {r4, pc}


WriteUInt:
	push {lr}

	mov r2, r0
	ldr r0, =buffer
	mov r1, #buffer_size
	bl UIntToString

	pop {lr}
	b WriteStringZ


WriteUIntBinary:
	push {lr}

	mov r2, r0
	ldr r0, =buffer
	mov r1, #buffer_size
	bl UIntToBinaryString

	pop {lr}
	b WriteStringZ


	.section .bss

	.align 4
	.set buffer_size, 36
buffer:	.fill buffer_size
buffer_end:

	.align 4
WriteCharProc:	.word 0

	.align 4
ReadCharProc:	.word 0
