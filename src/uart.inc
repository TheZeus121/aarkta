/* uart.inc -- UART MMIO constants. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.ifndef UART_INC_
	.set UART_INC_, 1

	.include "mmio.inc"

	/* PL011 UART */
	.set UART0_BASE, MMIO_BASE + 0x00201000

	/* Data Register */
	.set UART0_DR, UART0_BASE + 0x0

	/* Receive Status Register/Error Clear Register */
	.set UART0_RSRECR, UART0_BASE + 0x4

	/* Flag Register */
	.set UART0_FR, UART0_BASE + 0x18

	/* Not in Use */
	.set UART0_ILPR, UART0_BASE + 0x20

	/* Integer Baud Rate Divisor */
	.set UART0_IBRD, UART0_BASE + 0x24

	/* Fractional Baud Rate Divisor */
	.set UART0_FBRD, UART0_BASE + 0x28

	/* Line Control Register */
	.set UART0_LCRH, UART0_BASE + 0x2C

	/* Control Register */
	.set UART0_CR, UART0_BASE + 0x30

	/* Interrupt FIFO Level Select Register */
	.set UART0_IFLS, UART0_BASE + 0x34

	/* Interrupt Mask Set Clear Register */
	.set UART0_IMSC, UART0_BASE + 0x38

	/* Raw Interrupt Status Register */
	.set UART0_RIS, UART0_BASE + 0x3C

	/* Masked Interrupt Status Register */
	.set UART0_MIS, UART0_BASE + 0x40

	/* Interrupt Clear Register */
	.set UART0_ICR, UART0_BASE + 0x44

	/* DMA Control Register */
	.set UART0_DMACR, UART0_BASE + 0x48

	/* Test Control Register */
	.set UART0_ITCR, UART0_BASE + 0x80

	/* Integration Test Input Register */
	.set UART0_ITIP, UART0_BASE + 0x84

	/* Integration Test Output Register */
	.set UART0_ITOP, UART0_BASE + 0x88

	/* Test Data Register */
	.set UART0_TDR, UART0_BASE + 0x8C

	.endif			/* UART_INC_ */
