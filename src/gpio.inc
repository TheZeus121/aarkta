/* gpio.inc -- GPIO MMIO constants. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.ifndef GPIO_INC_
	.set GPIO_INC_, 1

	.include "mmio.inc"

	/* General Purpose I/O */
	.set GPIO_BASE, MMIO_BASE + 0x00200000

	/* GPIO Function Select 0 */
	.set GPIO_FSEL0, GPIO_BASE + 0x0

	/* GPIO Function Select 1 */
	.set GPIO_FSEL1, GPIO_BASE + 0x4

	/* GPIO Function Select 2 */
	.set GPIO_FSEL2, GPIO_BASE + 0x8

	/* GPIO Function Select 3 */
	.set GPIO_FSEL3, GPIO_BASE + 0xC

	/* GPIO Function Select 4 */
	.set GPIO_FSEL4, GPIO_BASE + 0x10

	/* GPIO Function Select 5 */
	.set GPIO_FSEL5, GPIO_BASE + 0x14

	/* GPIO Pin Output Set 0 */
	.set GPIO_SET0, GPIO_BASE + 0x1C

	/* GPIO Pin Output Set 1 */
	.set GPIO_SET1, GPIO_BASE + 0x20

	/* GPIO Pin Output Clear 0 */
	.set GPIO_CLR0, GPIO_BASE + 0x28

	/* GPIO Pin Output Clear 1 */
	.set GPIO_CLR1, GPIO_BASE + 0x2C

	/* GPIO Pin Level 0 */
	.set GPIO_LEV0, GPIO_BASE + 0x34

	/* GPIO Pin Level 1 */
	.set GPIO_LEV1, GPIO_BASE + 0x38

	/* GPIO Pin Event Detect Status 0 */
	.set GPIO_EDS0, GPIO_BASE + 0x40

	/* GPIO Pin Event Detect Status 1 */
	.set GPIO_EDS1, GPIO_BASE + 0x44

	/* GPIO Pin Rising Edge Detect Enable 0 */
	.set GPIO_REN0, GPIO_BASE + 0x4C

	/* GPIO Pin Rising Edge Detect Enable 1 */
	.set GPIO_REN1, GPIO_BASE + 0x50

	/* GPIO Pin Falling Edge Detect Enable 0 */
	.set GPIO_FEN0, GPIO_BASE + 0x58

	/* GPIO Pin Falling Edge Detect Enable 1 */
	.set GPIO_FEN1, GPIO_BASE + 0x5C

	/* GPIO Pin High Detect Enable 0 */
	.set GPIO_HEN0, GPIO_BASE + 0x64

	/* GPIO Pin High Detect Enable 1 */
	.set GPIO_HEN1, GPIO_BASE + 0x68

	/* GPIO Pin Low Detect Enable 0 */
	.set GPIO_LEN0, GPIO_BASE + 0x70

	/* GPIO Pin Low Detect Enable 1 */
	.set GPIO_LEN1, GPIO_BASE + 0x74

	/* GPIO Pin Asynchronous Rising Edge Detect 0 */
	.set GPIO_AREN0, GPIO_BASE + 0x7C

	/* GPIO Pin Asynchronous Rising Edge Detect 1 */
	.set GPIO_AREN1, GPIO_BASE + 0x80

	/* GPIO Pin Asynchronous Falling Edge Detect 0 */
	.set GPIO_AFEN0, GPIO_BASE + 0x88

	/* GPIO Pin Asynchronous Falling Edge Detect 1 */
	.set GPIO_AFEN1, GPIO_BASE + 0x8C

	/* GPIO Pin Pull-up/down Enable */
	.set GPIO_PUD, GPIO_BASE + 0x94

	/* GPIO Pin Pull-up/down Enable Clock 0 */
	.set GPIO_PUDCLK0, GPIO_BASE + 0x98

	/* GPIO Pin Pull-up/down Enable Clock 1 */
	.set GPIO_PUDCLK1, GPIO_BASE + 0x9C

	.endif			/* GPIO_INC_ */
