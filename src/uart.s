/* uart.s -- UART (serial port) stuff. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.section .text

	.globl InitUART
	.globl RecvChar
	.globl SendChar
	.globl SendStringZ

	.include "gpio.inc"
	.include "uart.inc"

InitUART:
	push {lr}

	/** Disable UART0. */
	mov r0, #0
	ldr r1, =UART0_CR
	str r0, [r1]

	/** Set GPIO ports 14 & 15 to Alternate function 0 */
	/* Read current value of selector. */
	ldr r1, =GPIO_FSEL1
	ldr r0, [r1]

	/* Clear out the FSEL14 & FSEL15 fields. */
	and r0, r0, #(~(0x3F << 12))

	/* Enable alternate function 0 on FSEL14 & FSEL15 fields. */
	orr r0, r0, #(0x24 << 12)

	/* Store the new value of selector. */
	ldr r1, =GPIO_FSEL1
	str r0, [r1]

	/** Disable pull-up/down for ports 14 & 15*/
	/* We're gonna be disabling stuff. */
	mov r0, #0
	ldr r1, =GPIO_PUD
	str r0, [r1]

	/* Wait for 150 cycles */
	mov r0, #150
	bl Delay

	/* The affected pins will be 14 & 15 */
	mov r0, #(3 << 14)
	ldr r1, =GPIO_PUDCLK0
	str r0, [r1]

	/* Wait for 150 cycles */
	mov r0, #150
	bl Delay

	/* Remove the clock */
	mov r0, #0
	ldr r1, =GPIO_PUDCLK0
	str r0, [r1]

	/** Clear any pending interrupts from UART */
	ldr r0, =0x7FF
	ldr r1, =UART0_ICR
	str r0, [r1]

	/** Set the baud rate */
	/* XXX: in raspi 3+ UART clock is dependent on system clock,
	should set the system clock freq if on that. */

	.set BAUD, 115200
	.set BASE_FREQ, 3000000
	.set IDIVI, BASE_FREQ / (BAUD * 16)
	.set FDIVI, (BASE_FREQ * 64 + BAUD * 8) / (BAUD * 16)

	ldr r0, =IDIVI
	ldr r1, =UART0_IBRD
	str r0, [r1]

	ldr r0, =FDIVI
	ldr r1, =UART0_FBRD
	str r0, [r1]

	/** Set the "encoding". */
	/* Buffering (1<<4) */
	/* 8bits (3<<5) */
	mov r0, #(7 << 5)
	ldr r1, =UART0_LCRH
	str r0, [r1]

	/** Disable all interrupts. */
	ldr r0, =0x7F2
	ldr r1, =UART0_IMSC
	str r0, [r1]

	/** Enable UART0, receiving & transferring. */
	ldr r0, =(1 | (1<<8) | (1<<9))
	ldr r1, =UART0_CR
	str r0, [r1]

	pop {pc}


RecvChar:
	ldr r1, =UART0_FR
0$:
	ldr r0, [r1]
	ands r0, r0, #(1 << 4)
	bne 0$

	ldr r1, =UART0_DR
	ldrb r0, [r1]

	mov pc, lr


SendChar:
	mov r3, r0
	ldr r1, =UART0_FR
0$:
	ldr r0, [r1]
	ands r0, r0, #(1 << 5)
	bne 0$

	ldr r1, =UART0_DR
	strb r3, [r1]

	mov pc, lr
