/* to-string.s -- procedures for converting values to strings. -*- asm -*-
 * Copyright © 2020  Uko Kokņevičs
 *
 * This file is part of ARM ARKTA.
 *
 * ARM ARKTA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * ARM ARKTA is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.
 */
	.text

	.globl SIntToString
	.globl UIntToString
	.globl UIntToBinaryString

SIntToString:
	cmp r2, #0
	beq ZeroToString
	bgt UIntToString

	push {r0, lr}
	/* negate r2 */
	rsb r2, r2, #0
	bl UIntToString

	pop {r2, lr}
	mov r3, #'-'

	cmp r0, r2
	strgtb r3, [r0, #-1]!

	add r1, r1, #1
	mov pc, lr


UIntToString:
	buffer .req r0
	buflen .req r1
	integer .req r2

	cmp integer, #0
	beq ZeroToString

	add buffer, buffer, buflen

	push {r4, r5, r6, r7, r8}

	char .req r3
	length .req r4
	divisor .req r5
	multipl .req r6

	tmpint .req r7
	garbage .req r8

	mov char, #0
	mov length, #0
	ldr divisor, =0x1999999A /* (2^32 / 10 + 1) */
	mov multipl, #10

0$:	cmp buflen, length
	strhib char, [buffer, #-1]!
	add length, length, #1

	cmp integer, #0
	beq 1$

	umull char, tmpint, integer, divisor
	/* tmpint = integer part */
	umull char, garbage, tmpint, multipl
	sub char, integer, char

	mov integer, tmpint
	add char, char, #'0'

	b 0$

1$:	mov buflen, length
	pop {r4, r5, r6, r7, r8}

	mov pc, lr

	.unreq buffer
	.unreq buflen
	.unreq integer
	.unreq char
	.unreq length
	.unreq divisor
	.unreq multipl
	.unreq tmpint
	.unreq garbage


UIntToBinaryString:
	buffer .req r0
	buflen .req r1
	integer .req r2

	cmp integer, #0
	beq ZeroToString

	add buffer, buffer, buflen

	push {r4}

	char .req r3
	length .req r4

	mov char, #0
	mov length, #0

0$:	cmp buflen, length
	strhib char, [buffer, #-1]!
	add length, length, #1

	cmp integer, #0
	beq 1$

	and char, integer, #1
	lsr integer, integer, #1

	add char, char, #'0'

	b 0$

1$:	mov buflen, length
	pop {r4}

	mov pc, lr

	.unreq buffer
	.unreq buflen
	.unreq integer
	.unreq char
	.unreq length


ZeroToString:
	cmp r1, #0
	cmpne r1, #1
	beq 0$

	add r0, r0, r1

	mov r2, #0
	strb r2, [r0, #-1]!

	mov r2, #'0'
	strb r2, [r0, #-1]!

0$:	mov r1, #2
	mov pc, lr
