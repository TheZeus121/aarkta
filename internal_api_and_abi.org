* ABI
  - r0, r1, r2, r3 :: Arguments & return values, not preserved
  - r4, r5, r6, r7, r8, r9, r10, r11, r12 :: Local variables, preserved
  - sp/r13 :: Stack pointer, preserved
  - lr/r14 :: Return address, not preserved
  - pc/r15 :: Program counter, set to the value of ~lr~ when procedure
              was called /unless/ the procedure is non-returning.

  Any registers not mentioned above are considered to be /undefined
  behaviour/ and you should not rely on them even being usable (for
  example, at the time of writing, hardware floating-point operations
  are not enabled).

  If arguments don't fit in the assigned registers, stack is used for
  the data that doesn't fit, in such cases the exact stack usage
  should be documented.  Stack is always cleaned by the caller (even
  when arguments are passed via stack).

  All /public/ procedures should be in ARM not THUMB, but this might
  change soon.
* API
** __start
*** Type
    () -> noreturn
*** File
    ~init.s~
*** Description
    You *SHOULD NOT* call this procedure.  This is a special procedure
    called during the boot procedure.  This procedure calls [[*Main]]
    (and, possibly, [[*Halt]]) in its process.
** Delay
*** Type
    (cycles) -> ()
*** File
    ~misc.s~
*** Description
    This procedure runs for an amount of time proportional to
    /cycles/.  I'm not sure about instruction timing, but I'd say it's
    /2*cycles+1/ processor cycles.
*** Possible problems
    While waiting, is essentially using 100% of the CPU by constantly
    looping.
** Halt
*** Type
    () -> noreturn
*** File
    ~misc.s~
*** Description
    Puts the current processor core in low-power mode and never
    returns.  This is the closest thing to a "shut-down" that AARKTA
    currently has.
** InitUART
*** Type
    () -> ()
*** File
    ~uart.s~
*** Description
    This procedure *SHOULD* be called from [[*Main]] before the use of any
    other UART procedures.  This procedure initializes the UART
    system.  Hard-coded defaults to 115200 baud, 8 bits, no parity, 1
    stop bit (unless I've messed something up).  Also enables the FIFO
    buffers.
** Main
*** Type
    () -> noreturn
*** File
    ~main.s~
*** Description
    You *SHOULD NOT* call this procedure.  This is a special procedure
    called by [[*__start]] during the boot procedure.  This procedure
    initializes all the subsystems.
** RecvChar
*** Type
    () -> (char)
*** File
    ~uart.s~
*** Description
    Receives one character via the UART0 port.  If no character exists
    in the FIFO buffer, blocks until receives a character.

    This procedure is fitting for [[*SetReadCharProc]].
*** Possible problems
    When waiting to receive a character is essentially using 100% of
    the CPU by constantly looping.
** ReadChar
*** Type
    () -> (char)
*** File
    ~io.s~
*** Description
    Reads a character using the procedure set by [[*SetReadCharProc]].
** SendChar
*** Type
    (char) -> ()
*** File
    ~uart.s~
*** Description
    Sends one character via the UART0 port.  If the FIFO buffer is
    full, blocks until there is free space.

    This procedure is fitting for [[*SetWriteCharProc]].
*** Possible problems
    When waiting for the FIFO buffer to empty is essentially using
    100% of the CPU by constantly looping.
** SetReadCharProc
*** Type
    (pptr: () -> (char)) -> ()
*** File
    ~io.s~
*** Description
    Sets the procedure to be used when calling [[*ReadChar]].
** SetWriteCharProc
*** Type
    (pptr: (char) -> ()) -> ()
*** File
    ~io.s~
*** Description
    Sets the procedure to be used when calling [[*WriteChar]].
** SIntToString
*** Type
    (buf, buf_len, int) -> (str_start, req_len)
*** File
    ~to-string.s~
*** Description
    Converts a signed integer to a zero-terminated string in decimal
    base.  Negative integers get a ~-~ prefix, while positive integers
    don't get any prefixes.

    See [[*UIntToString]] for a more in-depth explanation of behaviour &
    returned values.
** UIntToBinaryString
*** Type
    (buf, buf_len, int) -> (str_start, req_len)
*** File
    ~to-string.s~
*** Description
    Converts an unsigned integer to a zero-terminated string in binary
    base (no prefix).

    See [[*UIntToString]] for a more in-depth explanation of behaviour &
    returned values.
** UIntToString
*** Type
    (buf, buf_len, int) -> (str_start, req_len)
*** File
    ~to-string.s~
*** Description
    Converts an unsigned integer to a zero-terminated string in
    decimal base.

    If the buffer is too small, only converts the least-significant
    part of the number, but you should not rely on this.  In any case,
    returns the start of the converted string in ~r0~, this pointer
    will always be /inside/ of the buffer.  Returns in ~r1~ the
    minimal size of the buffer required to hold the string, useful for
    dynamically allocating the buffer.
** WriteChar
*** Type
    (char) -> ()
*** File
    ~io.s~
*** Description
    Writes a character using the procedure set by [[*SetWriteCharProc]].
** WriteNewLine
*** Type
    () -> ()
*** File
    ~io.s~
*** Description
    Writes a newline (CRLF).
** WriteSInt
*** Type
    (sint) -> ()
*** File
    ~io.s~
*** Description
    Writes a signed integer in the same format as [[*SIntToString]].
** WriteStringZ
*** Type
    (ptr) -> ()
*** File
    ~io.s~
*** Description
    Writes a zero-terminated string of characters.
** WriteUInt
*** Type
    (uint) -> ()
*** File
    ~io.s~
*** Description
    Writes an unsigned integer in the same format as [[*UIntToString]].
** WriteUIntBinary
*** Type
    (uint) -> ()
*** File
    ~io.s~
*** Description
    Writes an unsigned integer in the same format as
    [[*UIntToBinaryString]].
