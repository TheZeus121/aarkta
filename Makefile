# Makefile -- ARM ARKTA Makefile
# Copyright © 2020  Uko Kokņevičs
#
# This file is part of ARM ARKTA.
#
# ARM ARKTA is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ARM ARKTA is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ARM ARKTA.  If not, see <https://www.gnu.org/licenses/>.

GNU_PREFIX ?= arm-none-eabi
QEMU ?= qemu-system-arm

ASFLAGS ?=
QEMUFLAGS ?=

BUILD = build
SOURCE = src

ASFLAGS_ = $(ASFLAGS) -I $(SOURCE)
QEMUFLAGS_ = $(QEMUFLAGS) -M raspi2

TARGET = kernel7.img

LIST = kernel.list

MAP = kernel.map

LINKER = kernel.ld

SOURCES := $(wildcard $(SOURCE)/*.s)

OBJECTS := $(patsubst $(SOURCE)/%.s,$(BUILD)/obj/%.o,$(SOURCES))
DEPS := $(patsubst $(SOURCE)/%.s,$(BUILD)/dep/%.d,$(SOURCES))

# Generic targets

all: $(TARGET) $(LIST)

clean:
	-rm -rf $(BUILD)
	-rm -f $(TARGET)
	-rm -f $(LIST)
	-rm -f $(MAP)

love:
	@echo "not war"

qemu: $(TARGET)
	$(QEMU) $(QEMUFLAGS_) -kernel $(TARGET)

.PHONY: all clean love qemu

# Concrete targets

# ASM Listing
$(LIST): $(BUILD)/kernel.elf
	$(GNU_PREFIX)-objdump -D $< > $@

# Image file
$(TARGET): $(BUILD)/kernel.elf
	$(GNU_PREFIX)-objcopy $< -O binary $@

# ELF file
$(BUILD)/kernel.elf: $(OBJECTS) $(LINKER)
	$(GNU_PREFIX)-ld -O2 -z defs $(OBJECTS) -Map=$(MAP) -o $@ -T $(LINKER)

# Object files
$(BUILD)/obj/%.o: $(SOURCE)/%.s
	-@[ -d $(BUILD) ] || mkdir $(BUILD)
	-@[ -d $(BUILD)/obj ] || mkdir $(BUILD)/obj
	-@[ -d $(BUILD)/dep ] || mkdir $(BUILD)/dep
	$(GNU_PREFIX)-as $(ASFLAGS_) $< -o $@ --MD $(BUILD)/dep/$*.d

-include $(DEPS)
